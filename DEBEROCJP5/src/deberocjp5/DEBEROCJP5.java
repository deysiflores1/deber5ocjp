/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deberocjp5;

import java.util.Scanner;

/**
 *
 * @author Jorge
 */
public class DEBEROCJP5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
         /*Para la ejecucion correcta es necesario realizar los siguientes cambios: Clic derecho en el proyecto
        Seleccionar la opcion Propierties, seleccionar en la parte de Sources: Encoding escoger la opci�n ISO-8859-1
        Debido a que se puede presentar problemas de codificaci�n de caracteres */
        System.out.println("Escuela Politecnica Nacional");
        System.out.println("Deber de Oracle Certified Java Programmer");
        System.out.println("Autor:Deysi Tac�n");
        System.out.println("");
        
        //Ingreso de la cadena
        Scanner teclado= new Scanner(System.in);
        String ingresoCadena,cambioMin,quitarCaraEspeciales,sinAcento;
        evaluarPalindromo palindromoc=new evaluarPalindromo();
        System.out.print("Ingresa la cadena a evaluar:    ");
        ingresoCadena = teclado.nextLine();
        System.out.println("");
        
         //Cambiar de mayusculas a minusculas
        cambioMin=ingresoCadena.toLowerCase();
        System.out.println("Cambio de la cadena a minusculas:   \t"+cambioMin);
        
         //Quitar caracteres especiales: ",�,?,�,!"
        quitarCaraEspeciales=cambioMin.replace(",", "").replace(".", "").replace("?", "").replace("�", "").replace("�", "").replace("!", "").replace("'", "");
        System.out.println("La cadena sin caracteres especiales:   \t "+quitarCaraEspeciales);
        
        //Quitar el acento a la cadena
        sinAcento=quitarCaraEspeciales.replace('�','a').replace('�','e').replace('�','i').replace('�','o').replace('�','u');
        System.out.println("La cadena sin acento:       \t  \t "+sinAcento);
        System.out.println("");
        
        
        if(palindromoc.espalindromo1(sinAcento)){
            //Caso verdadero
                System.out.println("la cadena es palindroma");
                System.out.println(""+palindromoc.espalindromo1(sinAcento));
            }
        else{
                //Caso falso
                System.out.println("La cadena no es palindroma");
                 System.out.println(""+palindromoc.espalindromo1(sinAcento));
            }
    }
    
}
