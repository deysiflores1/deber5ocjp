/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deberocjp5;

/**
 *
 * @author Jorge
 */
public class evaluarPalindromo {//Permite verificar si la cadena es palindroma
    
     public boolean espalindromo1(String cadenaRec) {
        boolean retorno = true;
        int i, j;
        String cadenaMod = "";
//Quitar los espacios en blanco
        for (int rec = 0; rec < cadenaRec.length(); rec++) {
            if (cadenaRec.charAt(rec) != ' ') {
                cadenaMod += cadenaRec.charAt(rec);
            }
        } 
        
        //Se procede a igualar la cadena recibida con la cadena modificada(sin espacios)
        cadenaRec = cadenaMod;
        j = cadenaRec.length();
//Se procede a comparar la cadena, lo que tiene en e beginIndex y endIndex.
        for (i = 0; i < (cadenaRec.length()); i++) {
            if ((cadenaRec.substring(i, i + 1).equals(cadenaRec.substring(j - 1, j))) == false) {
//Devuelve false si no cumple alguno de los caracteres
                retorno = false;
                break;
            }
            j = j - 1;
        }
        return retorno;
    }
        
        

     
}
     